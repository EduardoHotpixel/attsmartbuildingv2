//
//  VideoPlayer.h
//  attSmartBuilding
//
//  Created by otto on 4/3/17.
//
//

#include "ofxHapPlayer.h"

class VideoPlayer {
    
public:
    
    VideoPlayer();
    ~VideoPlayer();
    VideoPlayer(int x, int y, string name, bool slidable);
    
    void setup(int x, int y, string name, bool slidable);
    void update();
    void draw();
    void play();
    void pause();
    
    void mouseMoved(ofMouseEventArgs & args);
    void mouseDragged(ofMouseEventArgs & args);
    void mousePressed(ofMouseEventArgs & args);
    void mouseReleased(ofMouseEventArgs & args);
    
    ofEvent<const VideoPlayer> onAnimationCompletion;
    
    ofxHapPlayer player;
    ofPoint pos;
    string videoName;
    bool slidable;
    bool playing;
    bool dragged;
    float alpha;
        
    bool debugMode;
};