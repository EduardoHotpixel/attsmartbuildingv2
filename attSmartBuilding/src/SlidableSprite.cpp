#include "SlidableSprite.h"

static float increment = 0.15;

// -----------------------------------------
SlidableSprite::SlidableSprite() {
}

// -----------------------------------------
SlidableSprite::~SlidableSprite() {
}

// -----------------------------------------
SlidableSprite::SlidableSprite(int x, int y, string path, string name) {
    setup(x, y, path, name);
}

// -----------------------------------------
void SlidableSprite::setup(int x, int y, string path, string name){
    ofDirectory dir;
    int hFiles = dir.listDir(path);
    for (int n = 0; n < hFiles; n++){
        if (n == 0) {
            sprite.load(dir.getPath(n));
        } else {
            sprite.addFile(dir.getPath(n));
        }
    }
    dragged = false;
    sprite.setAnchorPercent(0.5, 0.5);
    spriteName = name;
    currentFrame = 0;
    totalFrames = hFiles;
    pos.x = x;
    pos.y = y;
    alpha = 1.0;
    debugMode = false;
}

// -----------------------------------------
void SlidableSprite::update(){
    currentFrame = ofLerp(currentFrame, targetFrame, increment);
    sprite.setCurrentFrame(currentFrame);
}

// -----------------------------------------
void SlidableSprite::moveToFrame(float frame){
    targetFrame = frame * totalFrames;
}

// -----------------------------------------
void SlidableSprite::draw(){
//    cout << "CurrentFrame " << currentFrame << "\n";
//    cout << "TargetFrame " << targetFrame << "\n";
//    cout << "-------------------\n";
    ofEnableAlphaBlending();
    ofPushMatrix();
    ofTranslate(pos.x, pos.y);
    ofSetColor(255, 255, 255, alpha * 255);
    sprite.draw();
    ofPopMatrix();
 
    // Debug
    if (debugMode) {
        ofPushStyle();
        ofSetColor(ofColor::red);
        ofDrawCircle(pos.x, pos.y, 5);
        ofPopStyle();
    }
}

// -----------------------------------------
void SlidableSprite::mouseMoved(ofMouseEventArgs & args){
}

// -----------------------------------------
void SlidableSprite::mouseDragged(ofMouseEventArgs & args){
    if (dragged) {
//        cout << "Dragging " << spriteName << "\n";
        pos.x = args.x;
        pos.y = args.y;
    }
}

// -----------------------------------------
void SlidableSprite::mouseReleased(ofMouseEventArgs & args){
    dragged = false;
}

// -----------------------------------------
void SlidableSprite::mousePressed(ofMouseEventArgs & args){
    if (ofDist(args.x, args.y, pos.x, pos.y) < 5.0) {
//        cout << "mousePressed " << spriteName << "\n";
        dragged = true;
    }
}
