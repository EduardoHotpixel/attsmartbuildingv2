//
//  AnimatedSprite.h
//  AnimatedSprite
//
//  Created by iLalo on 29/04/14.
//
//

#include "ofxSprite.h"

class AnimatedSprite {
    
public:
    
    AnimatedSprite();
    ~AnimatedSprite();
    AnimatedSprite(int x, int y, string path, string name, bool play, bool loop);
    
    void setup(int x, int y, string path, string name, bool play, bool loop);
    void update();
    void draw();
    void fadeOut(float time);
    void fadeIn(float time);
    void play();
    void reverse();
    void pause();
    
    void mouseMoved(ofMouseEventArgs & args);
    void mouseDragged(ofMouseEventArgs & args);
    void mousePressed(ofMouseEventArgs & args);
    void mouseReleased(ofMouseEventArgs & args);
    
    ofEvent<const AnimatedSprite> onAnimationCompletion;
    ofEvent<const AnimatedSprite> onReverseAnimationCompletion;
    
    ofxSprite sprite;
    ofPoint pos;
    string path;
    string spriteName;
    bool playing;
    bool looping;
    bool backward;
    bool dragged;
    float alpha;
    
    bool fOut;
    float fOutTimer;
    
    bool fIn;
    float fInTimer;
    
    bool debugMode;
};
