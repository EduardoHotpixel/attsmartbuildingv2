#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup() {
    
    ofSetFrameRate(60);
    // Syphon stuff
    mainOutputSyphonServer.setName("ATTSmartBuilding");
    
    // OSC Stuff
    cout << "OSC port " << OSCPORT << "\n";
    oscReceiver.setup(OSCPORT);
    
    // Settings
    if (XML.loadFile("mySettings.xml")) {
        cout << "settings loaded\n";
    } else {
        cout << "unable to load settings\n";
    }
    
    grid.load("grid.png");
    
    backgroundColor.set(0, 0, 0);
    
    // Background
    backgroundVideo.load("background.mov");
    backgroundVideo.play();
    backgroundVideo.setPaused(true);
    
    // Security
    securityAVideo.load("securityA.mov");
    securityAVideo.setLoopState(OF_LOOP_NONE);
    securityAVideo.play();
    securityAVideo.setPaused(true);
    
    securityBVideo.load("securityB.mov");
    securityBVideo.setLoopState(OF_LOOP_NONE);
    securityBVideo.play();
    securityBVideo.setPaused(true);
    
    //Security Spanish
    securityCVideo.load("spanish/securityC.mov");
    securityCVideo.setLoopState(OF_LOOP_NONE);
    securityCVideo.play();
    securityCVideo.setPaused(true);
    
    //Security English
    securityCVideoEnglish.load("english/securityC.mov");
    securityCVideoEnglish.setLoopState(OF_LOOP_NONE);
    securityCVideoEnglish.play();
    securityCVideoEnglish.setPaused(true);
    
    // Pepper Spanish
    pepperAVideo.load("spanish/pepperA.mov");
    pepperAVideo.play();
    pepperAVideo.setPaused(true);
    
    pepperBVideo.load("spanish/pepperB.mov");
    pepperBVideo.play();
    pepperBVideo.setPaused(true);
    
    pepperCVideo.load("spanish/pepperC.mov");
    pepperCVideo.play();
    pepperCVideo.setPaused(true);
    
    
    // Pepper Engish
    pepperAVideoEnglish.load("english/pepperA.mov");
    pepperAVideoEnglish.play();
    pepperAVideoEnglish.setPaused(true);
    
    pepperBVideoEnglish.load("english/pepperB.mov");
    pepperBVideoEnglish.play();
    pepperBVideoEnglish.setPaused(true);
    
    pepperCVideoEnglish.load("english/pepperC.mov");
    pepperCVideoEnglish.play();
    pepperCVideoEnglish.setPaused(true);
    
    
    securityAplaying = false;
    securityBplaying = false;
    securityCplaying = false;
    
    currentFrame = 0;
    targetFrame = 0;
    
    gridMode = false;
    isEnglish = false;
    
    // setup the server to listen on 11999
    TCP.setup(7001);
    // optionally set the delimiter to something else.  The delimiter in the client and the server have to be the same, default being [/TCP]
    TCP.setMessageDelimiter("\n");
    lastSent = 0;
    
    
    //TCP LANG USER SERVER
    TCPLang.setup(7002);
    TCPLang.setMessageDelimiter("\n");
    lastSentLang = 0;

    showInfo = false;
    cout << "Setup done!" << "\n";
    
    
    jsonOK = false;
    queryEndTime = QUERY_INTERVAL;
    queryTimerEnd = false;
    queryStartTime = ofGetElapsedTimeMillis();

}

//--------------------------------------------------------------
void ofApp::update(){
    // Framerate
    ofSetWindowTitle(ofToString(ofGetFrameRate()));
    
    // Check OSC Messages
    while(oscReceiver.hasWaitingMessages()){
        ofxOscMessage m;
        oscReceiver.getNextMessage(&m);
        string msg_string = m.getAddress();
        if (msg_string == "/button") {
            securityAVideo.play();
            securityAplaying = true;
            securityBVideo.play();
            securityBplaying = true;
            
            if(isEnglish == false){
                securityCVideo.play();
                securityCplaying = true;
            }
            
            if(isEnglish == true){
                securityCVideoEnglish.play();
                securityCplayingEnglish = true;
            }
            
        } else if (msg_string == "/slider") {
            float pos = m.getArgAsFloat(0);
            if (pos >= 1.0) {
                pos = 0.999;
            }
            targetFrame = pos;
        }
        
        else if (msg_string == "/isEnglish"){
            isEnglish = true;
            cout<<"is english"<<endl;
            
        }
        
        else if (msg_string == "/isSpanish"){
            isEnglish = false;
            cout<<"is spanish"<<endl;
            
            
        }
    }
    
    //    currentFrame = ofLerp(currentFrame, targetFrame, 0.015);
    //    currentFrame = myLerp(currentFrame, targetFrame, 0.015);
    //    currentFrame = ofWrap(0.015, currentFrame, targetFrame);
    currentFrame = targetFrame;
    
    backgroundVideo.setPosition(currentFrame);
    
    if(isEnglish == false){
        pepperAVideo.setPosition(currentFrame);
        pepperBVideo.setPosition(currentFrame);
        pepperCVideo.setPosition(currentFrame);
    }
    
    if(isEnglish == true){
        pepperAVideoEnglish.setPosition(currentFrame);
        pepperBVideoEnglish.setPosition(currentFrame);
        pepperCVideoEnglish.setPosition(currentFrame);
    }
    
    
    // Update videos
    backgroundVideo.update();
    
    securityAVideo.update();
    securityBVideo.update();
    
    if(isEnglish == false){
        securityCVideo.update();
    }
    
    if(isEnglish == true){
        securityCVideoEnglish.update();
    }
    
    if (securityAVideo.getIsMovieDone() && securityAplaying) {
        securityAplaying = false;
        securityAVideo.setPaused(true);
        securityAVideo.setPosition(0);
    }
    if (securityBVideo.getIsMovieDone() && securityBplaying) {
        securityBplaying = false;
        securityBVideo.setPaused(true);
        securityBVideo.setPosition(0);
    }
    
    //Spanish
    if (isEnglish==false && securityCVideo.getIsMovieDone() && securityCplaying) {
        securityCplaying = false;
        securityCVideo.setPaused(true);
        securityCVideo.setPosition(0);
    }
    
    if(isEnglish == false){
        pepperAVideo.update();
        pepperBVideo.update();
        pepperCVideo.update();
    }
    
    
    //English
    if (isEnglish==true && securityCVideoEnglish.getIsMovieDone() && securityCplayingEnglish) {
        securityCplayingEnglish = false;
        securityCVideoEnglish.setPaused(true);
        securityCVideoEnglish.setPosition(0);
    }
    
    if(isEnglish == true){
        pepperAVideoEnglish.update();
        pepperBVideoEnglish.update();
        pepperCVideoEnglish.update();
    }
    
    
    float queryTimer = ofGetElapsedTimeMillis() - queryStartTime;
    
    if( queryTimer >= queryEndTime){
        
        queryTimerEnd= true;
        cout<<"query"<<endl;
        
    }
    
    if(queryTimer>=QUERY_INTERVAL){
        
        url = "http://10.0.1.128:8080/api/lang";
        httpJSON.open(url);
        
        if(httpJSON["lang"].asString()=="es"){
            jsonOK=false;
            isEnglish = false;
            cout<<"español"<<endl;
            
       
        }else{
            isEnglish = true;
            jsonOK = true;
            cout<<"ingles"<<endl;

           
        }
        cout<<"resets query"<<endl;
        queryTimerEnd = false;
        queryStartTime = ofGetElapsedTimeMillis();
        
    }

    
    
}

//--------------------------------------------------
float ofApp::myLerp(float start, float stop, float amt) {
    float r = start + amt;
    if (fabs(r - stop) < amt) {
        r = stop;
    }
    return r;
}

//--------------------------------------------------------------
void ofApp::draw(){
    ofBackground(backgroundColor);
    
    ofSetColor(255, 255, 255);
    // Draw videos
    if (backgroundVideo.isLoaded()) backgroundVideo.draw(0,0);
    
    if (securityAVideo.isLoaded()) securityAVideo.draw(478, 490);
    if (securityBVideo.isLoaded()) securityBVideo.draw(693, 276);
    
    if (isEnglish==false && securityCVideo.isLoaded()) securityCVideo.draw(820, 388);
    if (isEnglish==true && securityCVideoEnglish.isLoaded()) securityCVideoEnglish.draw(820, 388);
    
    
    if(isEnglish == false){
        if (pepperAVideo.isLoaded()) pepperAVideo.draw(1400,50);
        if (pepperBVideo.isLoaded()) pepperBVideo.draw(1500,290);
        if (pepperCVideo.isLoaded()) pepperCVideo.draw(1300,290);
    }
    
    if(isEnglish == true){
        ofPushStyle();
        ofDisableBlendMode();
        if (pepperAVideoEnglish.isLoaded()) pepperAVideoEnglish.draw(1400,50,262,225);
        ofPopStyle();
        if (pepperBVideoEnglish.isLoaded()) pepperBVideoEnglish.draw(1500,290,200,370);
        if (pepperCVideoEnglish.isLoaded()) pepperCVideoEnglish.draw(1300,290,200,370);
    }
    
    
    if (gridMode) {
        ofSetColor(255, 255, 255, 255);
        grid.draw(0, 0);
    }
    TCPServer();
    TCPServerLang();

    mainOutputSyphonServer.publishScreen();
}


//--------------------------------------------------------------
void ofApp::TCPServerLang(){
    
    ofSetHexColor(0xDDDDDD);
    
    // for each connected client lets get the data being sent and lets print it to the screen
    for(unsigned int i = 0; i < (unsigned int)TCPLang.getLastID(); i++){
        
        if( !TCPLang.isClientConnected(i) )continue;
        
        // calculate where to draw the text
        int xPos = 15;
        int yPos = 140 + (12 * i * 4);
        
        // get the ip and port of the client
        string port = ofToString( TCPLang.getClientPort(i) );
        string ip   = TCPLang.getClientIP(i);
        string info = "client "+ofToString(i)+" -connected from "+ip+" on port: "+port;
        
        
        // if we don't have a string allocated yet
        // lets create one
        if(i >= storeTextLang.size() ){
            storeTextLang.push_back( string() );
        }
        
        // receive all the available messages, separated by \n
        // and keep only the last one
        string str;
        string tmp;
        do{
            str = tmp;
            tmp = TCPLang.receive(i);
            
            
        }while(tmp!="");
        
        // if there was a message set it to the corresponding client
        if(str.length() > 0){
            storeTextLang[i] = str;
        }
        
        if(showInfo==true){
            //draw the info text and the received text bellow it
            ofDrawBitmapString(info, xPos, yPos);
            ofDrawBitmapString(storeTextLang[i], 25, yPos + 20);
        }
        jsonLang = storeTextLang[i];
        
    }
    
    //jsonLang="{lang : es}";
    
    std::string jsonElement[10];
    
    vector<string> splitJSON = ofSplitString( jsonLang, ":");
    
    for(int i=0; i<splitJSON.size(); i++){
        //     printf( "element %i is %s\n", i, splitJSON[i].c_str() );
        jsonElement[i] =splitJSON[i].c_str();
        
    }
    
    lang = jsonElement[1];
    lang.pop_back();
    //cout<<lang<<endl;
    
    if(lang.find("en") == true){
        
        isEnglish = true;
        
    }
    
    if(lang.find("es") == true){
        
        isEnglish = false;
        
    }

}
//--------------------------------------------------------------

void ofApp::TCPServer(){
    
    ofSetHexColor(0xDDDDDD);
    
    // for each connected client lets get the data being sent and lets print it to the screen
    for(unsigned int i = 0; i < (unsigned int)TCP.getLastID(); i++){
        
        if( !TCP.isClientConnected(i) )continue;
        
        // calculate where to draw the text
        int xPos = 15;
        int yPos = 80 + (12 * i * 4);
        
        // get the ip and port of the client
        string port = ofToString( TCP.getClientPort(i) );
        string ip   = TCP.getClientIP(i);
        string info = "client "+ofToString(i)+" -connected from "+ip+" on port: "+port;
        
        
        // if we don't have a string allocated yet
        // lets create one
        if(i >= storeText.size() ){
            storeText.push_back( string() );
        }
        
        // receive all the available messages, separated by \n
        // and keep only the last one
        string str;
        string tmp;
        do{
            str = tmp;
            tmp = TCP.receive(i);
            
            
        }while(tmp!="");
        
        // if there was a message set it to the corresponding client
        if(str.length() > 0){
            storeText[i] = str;
        }
        
        //draw the info text and the received text bellow it
        ofDrawBitmapString(info, xPos, yPos);
        ofDrawBitmapString(storeText[i], 25, yPos + 20);
        //json = storeText[i];
        
    }
    
    
    //  Pjson="{firstName:Martha,lastName: Jimenez,genre: ['f'],employer: Cocolab,data: {initial: 2500,transaction: 20,remaining: 1250,},reader: {index: [0],status: [0]}}";
    
    json="{firstName:Martha,lastName: Jimenez,genre: ['f'],employer: Cocolab,data: {initial: 2500,transaction: 20,remaining: 1250}}";
    
    std::string jsonElement[10];
    
    vector<string> splitJSON = ofSplitString( json, ",");
    
    for(int i=0; i<splitJSON.size(); i++){
        //printf( "element %i is %s\n", i, splitJSON[i].c_str() );
        jsonElement[i] =splitJSON[i].c_str();
        
    }
    
    firstName = jsonElement[0].erase(0,11);
    lastName = jsonElement[1].erase(0,9);
    genre = jsonElement[2].erase(0,7);
    employer = jsonElement[3].erase(0,9);
    initialData = jsonElement[4].erase(0,15);
    transactionData = jsonElement[5].erase(0,12);
    remainingData = jsonElement[6].erase(0,11);
    readerIndex = jsonElement[8].erase(0,16);
    //  readerStatus = jsonElement[9].erase(0,8);
    //cout<<firstName<<endl;
}

//------------------------------------------------------------------
void ofApp::keyPressed(int key){
    if (key == 'g'){
        gridMode = !gridMode;
    } else if (key == 's'){
        securityAVideo.play();
        securityAplaying = true;
        securityBVideo.play();
        securityBplaying = true;
        securityCVideo.play();
        securityCplaying = true;
    }
    
    if(key=='l'){
        isEnglish = ! isEnglish;
    
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
    int width = ofGetWidth();
    targetFrame = (float)x / (float)width;
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
    cout << "Mouse X: " << x << ", Y: " << y << "\n";
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){
}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){
}
