#pragma once

#include "ofMain.h"
#include "ofxSyphon.h"
#include "ofxOsc.h"
#include "ofxXmlSettings.h"
#include "ofxHapPlayer.h"
#include "ofxNetwork.h"
#include "ofxJSON.h"
#define QUERY_INTERVAL 30000

#define OSCPORT 3336

class ofApp : public ofBaseApp{
    
public:
    void setup();
    void update();
    void draw();
    
    float myLerp(float start, float stop, float amt);
    
    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void windowResized(int w, int h);
    void TCPServer();
    void TCPServerLang();


    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
    
    ofxSyphonServer mainOutputSyphonServer;
    ofxOscReceiver oscReceiver;
    ofxXmlSettings XML;
    ofColor backgroundColor;
    
    ofxHapPlayer backgroundVideo;
    
    ofxHapPlayer securityAVideo;
    ofxHapPlayer securityBVideo;
    ofxHapPlayer securityCVideo;
    ofxHapPlayer securityCVideoEnglish;

    
    ofxHapPlayer pepperAVideo;
    ofxHapPlayer pepperBVideo;
    ofxHapPlayer pepperCVideo;
    
    ofxHapPlayer pepperAVideoEnglish;
    ofxHapPlayer pepperBVideoEnglish;
    ofxHapPlayer pepperCVideoEnglish;
    
    float currentFrame;
    float targetFrame;
    
    bool securityAplaying;
    bool securityBplaying;
    bool securityCplaying;
    bool securityCplayingEnglish;
    
    bool gridMode;
    
    ofImage grid;
    
    int winX;
    int winY;
    
    bool isEnglish;
  
    //TCP SERVER
    ofxTCPServer TCP;
    vector <string> storeText;
    uint64_t lastSent;
    string  json;
    
    string firstName;
    string lastName;
    string genre;
    string employer;
    string initialData;
    string transactionData;
    string remainingData;
    string readerIndex;
    string readerStatus;
    
    //TCP LANG SERVER
    ofxTCPServer TCPLang;
    vector <string> storeTextLang;
    uint64_t lastSentLang;
    string  jsonLang;
    string lang;


    bool showInfo;
   
    bool jsonOK;
    std::string url;
    ofxJSON httpJSON;
    float queryStartTime;
    bool  queryTimerEnd;
    float queryEndTime;
    string httpLang;

  
};
