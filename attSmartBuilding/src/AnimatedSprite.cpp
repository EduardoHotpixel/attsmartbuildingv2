#include "AnimatedSprite.h"

// -----------------------------------------
AnimatedSprite::AnimatedSprite() {
}

// -----------------------------------------
AnimatedSprite::~AnimatedSprite() {
}

// -----------------------------------------
AnimatedSprite::AnimatedSprite(int x, int y, string path, string name, bool play, bool loop) {
    setup(x, y, path, name, play, loop);
}

// -----------------------------------------
void AnimatedSprite::setup(int x, int y, string path, string name, bool play, bool loop){
    ofDirectory dir;
    int hFiles = dir.listDir(path);
    for (int n = 0; n < hFiles; n++){
        if (n == 0) {
            sprite.load(dir.getPath(n));
        } else {
            sprite.addFile(dir.getPath(n));
        }
    }
    looping = loop;
    sprite.setLoop(looping);
    playing = play;
    backward = false;
    dragged = false;
    sprite.setAnchorPercent(0.5, 0.5);
    spriteName = name;
    pos.x = x;
    pos.y = y;
    alpha = 1.0;
    
    fOut = false;
    fIn = false;
    
    debugMode = false;
}

// -----------------------------------------
void AnimatedSprite::update(){
    if (fOut) {
        float a = 1.0 / (fOutTimer * 60.0);
        alpha -= a;
        if (alpha <= 0.0) {
            alpha = 0.0;
            fOut = false;
            sprite.stop();
            playing = false;
        }
    }
    if (fIn) {
        float a = 1.0 / (fInTimer * 60.0);
        alpha += a;
        if (alpha >= 1.0) {
            alpha = 1.0;
            fIn = false;
        }
    }
    if (playing) {
        if (!backward) {
            sprite.nextFrame();
            if (sprite.getCurrentFrame() == sprite.getTotalFrames() - 1) {
                ofNotifyEvent(onAnimationCompletion, *this);
                if (!looping) {
                    playing = false;
                    sprite.pause();
                }
            }
        } else {
            sprite.previousFrame();
            if (sprite.getCurrentFrame() == 0) {
                backward = false;
                playing = false;
                sprite.pause();
                ofNotifyEvent(onReverseAnimationCompletion, *this);
            }
        }
    }
}

// -----------------------------------------
void AnimatedSprite::draw(){
    ofEnableAlphaBlending();
    ofPushMatrix();
    ofTranslate(pos.x, pos.y);
    ofSetColor(255, 255, 255, alpha * 255);
    sprite.draw();
    ofPopMatrix();
 
    // Debug
    if (debugMode) {
        ofPushStyle();
        ofSetColor(ofColor::red);
        ofDrawCircle(pos.x, pos.y, 5);
        ofPopStyle();
    }
}

// -----------------------------------------
void AnimatedSprite::play(){
    if (playing) return;
    alpha = 1.0;
    playing = true;
    sprite.play();
}

// -----------------------------------------
void AnimatedSprite::reverse(){
    backward = true;
    playing = true;
    sprite.pause();
//    sprite.play();
}

// -----------------------------------------
void AnimatedSprite::fadeOut(float time){
    if (fIn) return;
    fOut = true;
    fOutTimer = time;
}

// -----------------------------------------
void AnimatedSprite::fadeIn(float time){
    if (fOut) return;
    fIn = true;
    fInTimer = time;
    playing = true;
}

// -----------------------------------------
void AnimatedSprite::pause(){
    playing = false;
    sprite.pause();
}

// -----------------------------------------
void AnimatedSprite::mouseMoved(ofMouseEventArgs & args){
}

// -----------------------------------------
void AnimatedSprite::mouseDragged(ofMouseEventArgs & args){
    if (dragged) {
//        cout << "Dragging " << spriteName << "\n";
        pos.x = args.x;
        pos.y = args.y;
    }
}

// -----------------------------------------
void AnimatedSprite::mouseReleased(ofMouseEventArgs & args){
    dragged = false;
}

// -----------------------------------------
void AnimatedSprite::mousePressed(ofMouseEventArgs & args){
    if (ofDist(args.x, args.y, pos.x, pos.y) < 5.0) {
//        cout << "mousePressed " << spriteName << "\n";
        dragged = true;
    }
}
