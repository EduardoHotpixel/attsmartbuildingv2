//
//  StaticSprite.cpp
//  infoVictoria1
//
//  Created by Otto on 8/19/15.
//
//

#include "StaticSprite.h"

// -----------------------------------------
StaticSprite::StaticSprite() {
}

// -----------------------------------------
StaticSprite::~StaticSprite() {
}

// -----------------------------------------
StaticSprite::StaticSprite(int x, int y, string path, string name) {
    setup(x, y, path, name);
}

// -----------------------------------------
void StaticSprite::setup(int x, int y, string path, string name){
    sprite.load(path);
    dragged = false;
    spriteName = name;
    pos.x = x;
    pos.y = y;
    alpha = 1.0;
    
    fOut = false;
    fIn = false;
    
    debugMode = false;
}

// -----------------------------------------
void StaticSprite::update(){
    if (fOut) {
        float a = 1.0 / (fOutTimer * 60.0);
        alpha -= a;
        if (alpha <= 0.0) {
            alpha = 0.0;
            fOut = false;
        }
    }
    if (fIn) {
        float a = 1.0 / (fInTimer * 60.0);
        alpha += a;
        if (alpha >= 1.0) {
            alpha = 1.0;
            fIn = false;
        }
    }
}

// -----------------------------------------
void StaticSprite::draw(){
    ofEnableAlphaBlending();
    ofPushMatrix();
    ofTranslate(pos.x - sprite.getWidth() / 2, pos.y - sprite.getHeight() / 2);
    ofSetColor(255, 255, 255, alpha * 255);
    sprite.draw(0, 0);
    ofPopMatrix();
    
    // Debug
    if (debugMode) {
        ofPushStyle();
        ofSetColor(ofColor::red);
        ofDrawCircle(pos.x, pos.y, 5);
        ofPopStyle();
    }
}

// -----------------------------------------
void StaticSprite::fadeOut(float time){
    if (fIn) return;
    fOut = true;
    fOutTimer = time;
}

// -----------------------------------------
void StaticSprite::fadeIn(float time){
    if (fOut) return;
    fIn = true;
    fInTimer = time;
}

// -----------------------------------------
void StaticSprite::mouseMoved(ofMouseEventArgs & args){
}

// -----------------------------------------
void StaticSprite::mouseDragged(ofMouseEventArgs & args){
    if (dragged) {
        //        cout << "Dragging " << spriteName << "\n";
        pos.x = args.x;
        pos.y = args.y;
    }
}

// -----------------------------------------
void StaticSprite::mouseReleased(ofMouseEventArgs & args){
    dragged = false;
}

// -----------------------------------------
void StaticSprite::mousePressed(ofMouseEventArgs & args){
    if (ofDist(args.x, args.y, pos.x, pos.y) < 5.0) {
        //        cout << "mousePressed " << spriteName << "\n";
        dragged = true;
    }
}
