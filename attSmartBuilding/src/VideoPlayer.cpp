//
//  VideoPlayer.cpp
//  attSmartBuilding
//
//  Created by otto on 4/3/17.
//
//

#include "VideoPlayer.h"

// -----------------------------------------
VideoPlayer::VideoPlayer() {
}

// -----------------------------------------
VideoPlayer::~VideoPlayer() {
}

// -----------------------------------------
VideoPlayer::VideoPlayer(int x, int y, string name, bool slidable) {
    setup(x, y, name, slidable);
}

// -----------------------------------------
void VideoPlayer::setup(int x, int y, string name, bool slidable){
    player.load(name);
    player.play();
//    player.setPaused(true);
    dragged = false;
    videoName = name;
    pos.x = x;
    pos.y = y;
    alpha = 1.0;
    debugMode = false;
}

// -----------------------------------------
void VideoPlayer::update(){
    if (player.isLoaded()) {
        player.update();
    }
    
    if (player.getIsMovieDone() && slidable && playing) {
        cout << "Settings saved!";
        player.setPaused(true);
        player.setPosition(0);
        playing = false;
    }
}

// -----------------------------------------
void VideoPlayer::draw(){
    if (player.isLoaded()) {
        player.draw(pos.x, pos.y);
    }
    // Debug
    if (debugMode) {
        ofPushStyle();
        ofSetColor(ofColor::red);
        ofDrawCircle(pos.x, pos.y, 5);
        ofPopStyle();
    }
}

// -----------------------------------------
void VideoPlayer::play(){
    if (playing) return;
    playing = true;
    player.play();
}


// -----------------------------------------
void VideoPlayer::mouseMoved(ofMouseEventArgs & args){
}

// -----------------------------------------
void VideoPlayer::mouseDragged(ofMouseEventArgs & args){
    if (dragged) {
        //        cout << "Dragging " << spriteName << "\n";
        pos.x = args.x;
        pos.y = args.y;
    }
}

// -----------------------------------------
void VideoPlayer::mouseReleased(ofMouseEventArgs & args){
    dragged = false;
}

// -----------------------------------------
void VideoPlayer::mousePressed(ofMouseEventArgs & args){
    if (ofDist(args.x, args.y, pos.x, pos.y) < 5.0) {
        //        cout << "mousePressed " << spriteName << "\n";
        dragged = true;
    }
}
