//
//  SlidableSprite.h
//  SlidableSprite
//
//  Created by iLalo on 29/04/14.
//
//

#include "ofxSprite.h"

class SlidableSprite {
    
public:
    
    SlidableSprite();
    ~SlidableSprite();
    SlidableSprite(int x, int y, string path, string name);
    
    void setup(int x, int y, string path, string name);
    void update();
    void draw();
    void moveToFrame(float frame);
    
    void mouseMoved(ofMouseEventArgs & args);
    void mouseDragged(ofMouseEventArgs & args);
    void mousePressed(ofMouseEventArgs & args);
    void mouseReleased(ofMouseEventArgs & args);
    
    ofEvent<const SlidableSprite> onAnimationCompletion;
    ofEvent<const SlidableSprite> onReverseAnimationCompletion;
    
    ofxSprite sprite;
    ofPoint pos;
    string path;
    string spriteName;
    float currentFrame;
    float targetFrame;
    int totalFrames;
    bool dragged;
    float alpha;
    bool debugMode;
    
};
