//
//  StaticSprite.h
//  infoVictoria1
//
//  Created by Otto on 8/19/15.
//
//

#include "ofMain.h"

class StaticSprite {
    
public:
    StaticSprite();
    ~StaticSprite();
    StaticSprite(int x, int y, string path, string name);
    
    void setup(int x, int y, string path, string name);
    void update();
    void draw();
    void fadeOut(float time);
    void fadeIn(float time);
    
    void mouseMoved(ofMouseEventArgs & args);
    void mouseDragged(ofMouseEventArgs & args);
    void mousePressed(ofMouseEventArgs & args);
    void mouseReleased(ofMouseEventArgs & args);
    
    ofImage sprite;
    ofPoint pos;
    string path;
    string spriteName;

    bool dragged;
    float alpha;
    
    bool fOut;
    float fOutTimer;
    
    bool fIn;
    float fInTimer;
    
    bool debugMode;
};